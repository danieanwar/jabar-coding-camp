//Soal 1
var nilai = 90;

if(nilai < 55){
    console.log("E");
} else if(nilai >= 55 && nilai < 65){
    console.log("D");
} else if(nilai >= 65 && nilai < 75){
    console.log("C");
} else if(nilai >= 75 && nilai < 85){
    console.log("B");
} else if(nilai >= 85){
    console.log("A");
}

//Soal 2

var tanggal = 5;
var bulan = 11;
var tahun = 1994;
var bulanString = " ";
var outputTanggal = false;
var outputBulan = false;

if(tanggal < 1){
    console.log("Tanggal kurang dari jumlah hari yang seharusnya!");
} 
else if(tanggal > 31){
    console.log("Tanggal lebih dari jumlah hari yang seharusnya!");
} 
else {
    outputTanggal = true;
}

switch(bulan){
    case 1  : {
        bulanString = "Januari"; 
        outputBulan = true;
        break;}
    case 2  : {
        bulanString = "Februari"; 
        outputBulan = true;
        break;}
    case 3  : {
        bulanString = "Maret"; 
        outputBulan = true;
        break;}
    case 4  : {
        bulanString = "April"; 
        outputBulan = true;
        break;}
    case 5  : {
        bulanString = "Mei"; 
        outputBulan = true;
        break;}
    case 6  : {
        bulanString = "Juni"; 
        outputBulan = true;
        break;}
    case 7  : {
        bulanString = "Juli"; 
        outputBulan = true;
        break;}
    case 8  : {
        bulanString = "Agustus"; 
        outputBulan = true;
        break;}
    case 9  : {
        bulanString = "September"; 
        outputBulan = true;
        break;}
    case 10 : {
        bulanString = "Oktober"; 
        outputBulan = true;
        break;}
    case 11 : {
        bulanString = "November"; 
        outputBulan = true;
        break;}
    case 12 : {
        bulanString = "Desember"; 
        outputBulan = true;
        break;}
    default : { 
        console.log("Bulan tidak termasuk dari jumlah bulan yang seharusnya!");
        outputBulan = false;
        }
}

if((outputTanggal = true) && (outputBulan = true)){
    console.log(tanggal,bulanString,tahun)
}

//Soal 3
var n = 7;
var y;
var x;
var garis = "#";
var garisBaru = "";

for(y = 1; y <= n; y++){

    for(x = 0; x < y; x++){
        garisBaru = garisBaru + garis;
    }
      
x = 0;
console.log(garisBaru);
garisBaru = "";
}

//Soal 4
var x = 1;
var m = 5;
var kelipatanString = "";

    for(var iterasi=1; iterasi <= m; iterasi++ ){
        
        if((x%3)==0){
           kelipatanString = "I love VueJS";
        }
        else if((x%2)==0){
            kelipatanString = "I love Javascript";
        }
        else if((x%2)==1){
            kelipatanString = "I love programming";
        }

        console.log(iterasi ,"- " + kelipatanString);

        if(x >= 3){
            x = 0;
        }
        x++
    }