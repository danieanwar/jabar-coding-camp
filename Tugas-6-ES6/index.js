// Soal 1
const hitungPersegi = (panjang, lebar) => {
    return {
        luasPersegi: function(){
            console.log("Luas Persegi = ", panjang * lebar)
        },
        kelilingPersegi: function(){
            console.log("Keliling Persegi = ", (2 * panjang) + (2 * lebar))
        }
    } 
}

const panjang = 8;
const lebar = 10;
hitungPersegi(panjang,lebar).luasPersegi()
hitungPersegi(panjang,lebar).kelilingPersegi()

// Soal 2
const newFunction = (firstName, lastName) => {
    return {
      firstName: firstName,
      lastName: lastName,
      fullName: function(){
        console.log(firstName + " " + lastName)
      }
    }
  }
   
  //Driver Code 
  newFunction("William", "Imoh").fullName() 

// Soal 3
const newObject = {
    firstName: "Muhammad",
    lastName: "Iqbal Mubarok",
    address: "Jalan Ranamanyar",
    hobby: "playing football"
  }

const {firstName, lastName, address, hobby} = newObject;
console.log(firstName, lastName, address, hobby)

// Soal 4
const west = ["Will", "Chris", "Sam", "Holly"]
const east = ["Gill", "Brian", "Noel", "Maggie"]
const combined = [...west, ...east];
console.log(combined);

// Soal 5
const planet = "earth" 
const view = "glass" 
var before = 'Lorem ' + view + ' dolor sit amet, ' + 'consectetur adipiscing elit,' + planet 
const after = {before}
console.log(after)


