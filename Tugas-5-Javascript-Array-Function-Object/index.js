// Soal 1
var daftarHewan = ["2. Komodo", "5. Buaya", "3. Cicak", "4. Ular", "1. Tokek"];
daftarHewan.sort();
var i = daftarHewan.length;

for(x = 0; x < i; x++){
    console.log(daftarHewan[x]);
}

// Soal 2

function introduce(input){
    return "nama saya " + input['name'] + ", umur saya " + input['age'] + ", alamat saya " + input['address'] + ", dan saya punya hobby yaitu " + input['hobby'];
}

var data = {name : "John" , age : 30 , address : "Jalan Pelesiran" , hobby : "Gaming" }
 
var perkenalan = introduce(data)
console.log(perkenalan)

// Soal 3

function hitung_huruf_vokal(input){
    var vocal = input.match(/[aeiou]/gi);
    return vocal.length;
}

var hitung_1 = hitung_huruf_vokal("Muhammad")

var hitung_2 = hitung_huruf_vokal("Iqbal")

console.log(hitung_1 , hitung_2) // 3 2

// Soal 4
function hitung(input) {
    
     if(input==0){
        x = input - 2;
        return x;
     }
     else if((input%5)===0){
        x = input + 3;
        return x;
     }
     else if((input%3)==0){
        x = input + 1;
        return x;
     }
     else if((input%2)==0){
        return input;
     }
     else if((input%2)==1){
        x = input - 1;
        return x;
     }
}

console.log( hitung(0));
console.log( hitung(1));
console.log( hitung(2));
console.log( hitung(3));
console.log( hitung(5));