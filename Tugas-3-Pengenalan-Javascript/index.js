//soal 1

var pertama = "saya sangat senang hari ini";
var kedua = "belajar javascript itu keren";
var upper = kedua.toUpperCase()

var gabungan = pertama.substr(0, 4) + " " + pertama.substr(12, 7) + kedua.substr(0, 7) + " " + upper.substr(8, 10);
console.log(gabungan);

//soal 2
var kataPertama = parseInt("10");
var kataKedua = parseInt("2");
var kataKetiga = parseInt("4");
var kataKeempat = parseInt("6");

console.log(kataPertama + kataKedua * kataKetiga + kataKeempat);

//soal 3
var kalimat = 'wah javascript itu keren sekali'; 
var kataPertama = kalimat.substring(0, 3); 
var kataKedua = kalimat.substring(4, 14); // do your own! 
var kataKetiga = kalimat.substring(15, 18); // do your own! 
var kataKeempat = kalimat.substring(19, 24); // do your own! 
var kataKelima = kalimat.substring(25, 31); // do your own! 

console.log('Kata Pertama: ' + kataPertama); 
console.log('Kata Kedua: ' + kataKedua); 
console.log('Kata Ketiga: ' + kataKetiga); 
console.log('Kata Keempat: ' + kataKeempat); 
console.log('Kata Kelima: ' + kataKelima);